#!/bin/sh
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

export NAME=$(basename $DIR)
docker build $DIR --tag $NAME