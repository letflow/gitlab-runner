#!/bin/bash

# gitlab-ci-multi-runner data directory
DATA_DIR="/etc/gitlab-runner"
CONFIG_FILE=${CONFIG_FILE:-$DATA_DIR/config-$HOSTNAME.toml}
export CONFIG_FILE

TOTAL_MEMORY=$(grep MemTotal /proc/meminfo | awk '{print $2}') # in KB
KB2GB=$((1024*1024))
TOTAL_MEMORY=$(((TOTAL_MEMORY+KB2GB-1)/KB2GB)) # in GB

TAG_LIST="$TAG_LIST,mem-${TOTAL_MEMORY}"

TOKEN=$(curl --silent --request POST --url "$CI_SERVER_URL/api/v4/user/runners" \
    --data "runner_type=instance_type" \
    --data "description=Docker runner" \
    --data "tag_list=${TAG_LIST}" \
    --header "PRIVATE-TOKEN: ${GITLAB_PERSONAL_ACCESS_TOKEN}" | jq -r .token)

# auto register runner
gitlab-runner register --non-interactive --token=${TOKEN}
sed -i -e "s/concurrent = .*$/concurrent = 10/g" $CONFIG_FILE

bash /entrypoint "$@"

# auto unregister runner
gitlab-runner unregister