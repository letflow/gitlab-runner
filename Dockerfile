FROM gitlab/gitlab-runner:alpine-v17.1.1

# install docker
RUN apk --update --no-cache add docker jq

COPY entrypoint.sh /entrypoint-daemon

ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint-daemon"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]